# Caladont

[Caladont](https://caladont.com) is a fun, word chaining game where new word should start with last two letters of previous word.

Rules:

* The word can be used only once
* You have 3 tries
* You'll have 5-25 seconds to respond

You will win if:

* You are first to use a word Caladont
* The player after you has no words to continue

## Issue Tracker

This repository contains the [central issue tracker](https://bitbucket.org/hippiegames/caladont/issues) for the Caladont game.
